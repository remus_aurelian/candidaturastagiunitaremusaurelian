
import java.util.Scanner;

public class Triangle implements Figure{
	private double lat1,lat2,lat3;

	public Triangle(){lat1=0;lat2=0;lat3=0;}
	public Triangle(double arg1,double arg2,double arg3){lat1=arg1;lat2=arg2;lat3=arg3;}

	public double Area(){
		double semiPer=(lat1+lat2+lat3)/2;
		return Math.sqrt(semiPer*(semiPer-lat1)*(semiPer-lat2)*(semiPer-lat3));
	}

	public double Perimeter(){
		return lat1+lat2+lat3;
	}
	
	public void inputValues(){
		Scanner scan=new Scanner(System.in);
		System.out.println("Cele trei laturi: ");

		this.lat1=scan.nextDouble();
		this.lat2=scan.nextDouble();
		this.lat3=scan.nextDouble();
		scan.close();
	}
}
