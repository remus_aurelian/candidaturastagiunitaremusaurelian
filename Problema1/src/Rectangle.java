
import java.util.Scanner;

public class Rectangle implements Figure{
	private double length, width;
	
	public Rectangle(){length=0;width=0;}
	public Rectangle(double arg1,double arg2){length=arg1;width=arg2;}
	
	public double Area(){
		return length*width;
	}
	
	public double Perimeter(){
		return length*2+width*2;
	}
	
	public void inputValues(){
		Scanner scan=new Scanner(System.in);
		System.out.println("Lungimea si latimea: ");
		
		this.length=scan.nextDouble();
		this.width=scan.nextDouble();
		scan.close();
	}
}
