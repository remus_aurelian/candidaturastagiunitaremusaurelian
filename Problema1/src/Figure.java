
public interface Figure {
	public double Area();
	public double Perimeter();
	public void inputValues();
}

