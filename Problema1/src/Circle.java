
import java.util.*;

public class Circle implements Figure{
	private double rad;

	public Circle(){rad=0;}
	public Circle(double arg){rad=arg;}

	public double Area(){
		return 3.1415*rad*rad;
	}

	public double Perimeter(){
		return 2*3.1415*rad;
	}

	public void inputValues(){
		Scanner scan=new Scanner(System.in);
		System.out.println("Raza: ");
		
		this.rad=scan.nextDouble();
		scan.close();
	}
}
