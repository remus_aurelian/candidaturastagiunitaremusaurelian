import java.io.*;

public class Problema1 {

	static public void Output(Figure fig){
		System.out.println("Area: "+fig.Area()+" Perimeter: "+fig.Perimeter());
	}
	

	public static void main(String[] args) {
		BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
		try{
			System.out.println("Alege figura (cerc/patrulater/triunghi): ");
			String choice=in.readLine();
			
			switch(choice){
			case "cerc":
				Circle cerc=new Circle();
				cerc.inputValues();
				Output(cerc);
				break;
			case "patrulater":
				Rectangle rect=new Rectangle();
				rect.inputValues();
				Output(rect);
				break;
			case "triunghi":
				Triangle triunghi=new Triangle();
				triunghi.inputValues();
				Output(triunghi);
				break;
			default:
				System.out.println("Figura incorecta");
			}
		
		}catch(IOException e){
			System.out.println("ERROR");
		}
	}
}
