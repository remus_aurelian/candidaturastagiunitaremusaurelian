import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problema2{
	
	static int[] convert(String str){
		String[] aux=str.split("[ ,]+");
		int[] numbers=new int[aux.length];
		for(int i=0;i<aux.length;i++)
			numbers[i]=Integer.parseInt(aux[i]);
		return numbers;
	}

	static int minNumber(int[] arr){
		int minim=arr[0];
		for(int i=1;i<arr.length;i++){
			if(minim>arr[i])
				minim=arr[i];
		}
		return minim;
	}

	static int maxNumber(int[] arr){
		int maxim=arr[0];
		for(int i=1;i<arr.length;i++){
			if(maxim<arr[i])
				maxim=arr[i];
		}
		return maxim;
	}

	static boolean isPalindrome(int arg){
		int rev=0,aux=arg;
		while(aux!=0){
			rev=rev*10+aux%10;
			aux=aux/10;
		}
		if(rev==arg)
			return true;
		else
			return false;
	}

	static boolean isPrime(int arg){
		for(int i=2;i<=(int)Math.sqrt(arg);i++)
			if(arg%i==0)
				return false;
		return true;
	}
	
	static String getPalindromes(int[] arr){
		String pal=new String("");
		for(int i=0;i<arr.length;i++){
			if(isPalindrome(arr[i]))
				pal=pal+Integer.toString(arr[i])+" ";
		}
		return pal;
	}

	static String getPrimeNumbers(int[] arr){
		String primes=new String();
		for(int i=0;i<arr.length;i++){
			if(isPrime(arr[i]))
				primes=primes+Integer.toString(arr[i])+" ";
		}
		return primes;
	}
	
	public static void main(String[] args) {
		BufferedReader in=new BufferedReader(new InputStreamReader(System.in));

		try{
			String numbersaux=new String();
			System.out.print("Introduceti sirul de numere: ");
			numbersaux=in.readLine();

			int[] numbers=convert(numbersaux);

			System.out.println("Smallest number: "+minNumber(numbers));
			System.out.println("Largest number: "+maxNumber(numbers));
			System.out.println("Palindromes: "+getPalindromes(numbers));
			System.out.println("Prime numbers: "+getPrimeNumbers(numbers));

		}catch(IOException e){
			System.out.println("!!!Error !!!");
		}catch(NumberFormatException e){
			System.out.println("Sirul nu contine numere");
		}
	}
}